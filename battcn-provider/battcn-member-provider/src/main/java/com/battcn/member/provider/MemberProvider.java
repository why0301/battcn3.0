package com.battcn.member.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Levin
 * @since 2018/3/8 0008
 */
@SpringBootApplication
public class MemberProvider {

    public static void main(String[] args) {
        SpringApplication.run(MemberProvider.class, args);
    }
}
