package com.battcn.framework.webmagic.downloader;

import com.google.common.collect.Maps;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.annotation.Contract;
import org.apache.http.annotation.ThreadingBehavior;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Request;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.downloader.AbstractDownloader;
import us.codecraft.webmagic.downloader.HttpClientGenerator;
import us.codecraft.webmagic.downloader.HttpClientRequestContext;
import us.codecraft.webmagic.downloader.HttpUriRequestConverter;
import us.codecraft.webmagic.proxy.Proxy;
import us.codecraft.webmagic.proxy.ProxyProvider;
import us.codecraft.webmagic.selector.PlainText;
import us.codecraft.webmagic.utils.CharsetUtils;
import us.codecraft.webmagic.utils.HttpClientUtils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author yanglei 2018年1月22日
 *         <p>
 *         自己改装的Downloader 因为自带的可能会出现readtime out soket_time out connect time
 *         out 等等待时间过长的问题 而且无法设置时间间隔
 */
@Contract(threading = ThreadingBehavior.SAFE)
public class CrawlerDownloader extends AbstractDownloader {

    private static final String DEFAULT_CHARSET = "utf-8";
    /**
     * 错误地址
     */
    private static final Map<String, String> ERROR_REQUEST_URL = Maps.newHashMap();

    /**
     * 无效代理ip
     */
    private static final Map<Proxy, Integer> INVALID_PROXY = Maps.newHashMap();

    /**
     * 无效代理ip次数统计
     */
    private static final AtomicInteger INVALID_PROXY_COUNT = new AtomicInteger(1);

    private static Logger logger = LoggerFactory.getLogger(CrawlerDownloader.class);

    private final Map<String, CloseableHttpClient> httpClients = Maps.newHashMap();

    private HttpClientGenerator httpClientGenerator = new HttpClientGenerator();

    private HttpUriRequestConverter httpUriRequestConverter = new HttpUriRequestConverter();

    private ProxyProvider proxyProvider;


    public void setHttpUriRequestConverter(HttpUriRequestConverter httpUriRequestConverter) {
        this.httpUriRequestConverter = httpUriRequestConverter;
    }

    public void setProxyProvider(ProxyProvider proxyProvider) {
        this.proxyProvider = proxyProvider;
    }

    private CloseableHttpClient getHttpClient(Site site) {
        if (site == null) {
            throw new NullPointerException("Site 不能为空");
        }
        String domain = site.getDomain();
        CloseableHttpClient httpClient = httpClients.get(domain);
        if (httpClient == null) {
            synchronized (this) {
                httpClient = httpClients.computeIfAbsent(domain, k -> httpClientGenerator.getClient(site));
            }
        }
        return httpClient;
    }

    @Override
    public Page download(Request request, Task task) {
        if (task == null || task.getSite() == null) {
            throw new NullPointerException("task or site can not be null");
        }
        CloseableHttpResponse httpResponse = null;
        CloseableHttpClient httpClient = getHttpClient(task.getSite());
        Proxy proxy = null;
        if (proxyProvider != null) {
            proxy = proxyProvider.getProxy(task);
        }
        //将request和site和代理ip转换可用的HttpUriRequest和HttpClientContext以供httpClient执行使用
        HttpClientRequestContext requestContext = httpUriRequestConverter.convert(request, task.getSite(), proxy);
        Page page = Page.fail();
        try {
            Long startTime = System.currentTimeMillis();

            httpResponse = httpClient.execute(requestContext.getHttpUriRequest(), requestContext.getHttpClientContext());
            logger.info("所用时间 {}", System.currentTimeMillis() - startTime);
            page = handleResponse(request, request.getCharset() != null ? request.getCharset() : task.getSite().getCharset(), httpResponse, task);
            onSuccess(request);
            logger.info("downloading page success {}", request.getUrl());
            return page;
        } catch (IOException e) {
            logger.warn("download page {} error", request.getUrl(), e);
            onError(request);
            return page;
        } finally {
            if (httpResponse != null) {
                //确保连接被释放到池中
                EntityUtils.consumeQuietly(httpResponse.getEntity());
            }
            if (proxyProvider != null && proxy != null) {
                proxyProvider.returnProxy(proxy, page, task);
            }
        }
    }


    private Page handleResponse(Request request, String charset, HttpResponse httpResponse, Task task) throws IOException {
        byte[] bytes = IOUtils.toByteArray(httpResponse.getEntity().getContent());
        String contentType = httpResponse.getEntity().getContentType() == null ? "" : httpResponse.getEntity().getContentType().getValue();
        Page page = new Page();
        page.setBytes(bytes);
        if (!request.isBinaryContent()) {
            if (charset == null) {
                charset = getHtmlCharset(contentType, bytes);
            }

            if (!StringUtils.containsIgnoreCase(contentType, DEFAULT_CHARSET)) {
                charset = "GBK";
            }
            page.setCharset(charset);
            page.setRawText(new String(bytes, charset));
        }
        page.setUrl(new PlainText(request.getUrl()));
        page.setRequest(request);
        page.setStatusCode(httpResponse.getStatusLine().getStatusCode());
        page.setDownloadSuccess(true);
        page.setHeaders(HttpClientUtils.convertHeaders(httpResponse.getAllHeaders()));
        return page;
    }

    private String getHtmlCharset(String contentType, byte[] contentBytes) throws IOException {
        String charset = CharsetUtils.detectCharset(contentType, contentBytes);
        if (charset == null) {
            charset = Charset.defaultCharset().name();
            logger.warn("Charset autodetect failed, use {} as charset. Please specify charset in Site.setCharset()", Charset.defaultCharset());
        }
        return charset;
    }

    @Override
    public void setThread(int thread) {
        httpClientGenerator.setPoolSize(thread);
    }


    @Override
    protected void onSuccess(Request request) {
        String url = request.getUrl();
        if (ERROR_REQUEST_URL.get(url) != null) {
            ERROR_REQUEST_URL.remove(url);
        }
    }

    @Override
    protected void onError(Request request) {
        String url = request.getUrl();
        ERROR_REQUEST_URL.putIfAbsent(url, url);
        logger.info("现在有 {} 条链接跳转失败", ERROR_REQUEST_URL.toString());
        logger.info("现在有 {} 条链接跳转失败", ERROR_REQUEST_URL.size());
    }

}

